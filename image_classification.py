import numpy as np
import cv2

# Load the classes
all_rows = open('models/synset_words.txt').read().strip().split("\n")
classes = [r[r.find(' ') + 1:] for r in all_rows]

images = ['crab.jpg', 'typewriter.jpg', 'keyboard.jpg', 'keyboard_music.jpg' ]

# Load Model
net = cv2.dnn.readNetFromCaffe('models/bvlc_googlenet.prototxt','models/bvlc_googlenet.caffemodel')

for image in images:

	# Load and resize image
	img = cv2.imread('images/' + image)
	img = cv2.resize(img, (500, 400))
	
	# Pass image to model and get class probability
	blob = cv2.dnn.blobFromImage(img, 1, (224,224))
	net.setInput(blob)
	outp = net.forward()

	# Sort output descending
	idx = np.argsort(outp[0])[::-1][:5]

	# Prepare image label
	top_class = classes[idx[0]]
	prob = outp[0][idx[0]]*100
	img_label = top_class + ' with prob ' + '{:.2f}'.format(prob) + '%'

	# Print top probabilities
	for (i,id) in enumerate(idx):
	    print('{}. {} ({}): Probability {:.3}%'.format(i+1, classes[id], id, outp[0][id]*100))

	# Show the image with label
	cv2.imshow(img_label, img)

cv2.waitKey(0)
cv2.destroyAllWindows()
