import wget

# Config
url = 'https://github.com/pjreddie/darknet/blob/master/cfg/yolov3.cfg'
destination = 'models/YOLOv3/yolov3.cfg'
wget.download(url, out=destination)

# Weight
url = 'https://pjreddie.com/media/files/yolov3.weights'
destination = 'models/YOLOv3/yolov3.weights'
wget.download(url, out=destination)

