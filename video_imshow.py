import numpy as np
import cv2

cap = cv2.VideoCapture('videos/road_trip.mov')

if cap.isOpened() == False:
    print('Cannot open file or video stream')


while True:
	ret, frame = cap.read()

	if ret == True:

		height, width, layers = frame.shape
		new_h = height // 4
		new_w = width // 4
		frame = cv2.resize(frame, (new_w, new_h))

		cv2.imshow('Frame', frame)

		if cv2.waitKey(25) & 0xFF == 27:
			break
	else:
		break

cap.release()
cv2.destroyAllWindows()